import axios from "axios";
import { readFileSync, writeFileSync } from "fs";

const dbPath = new URL("../Db/db.json", import.meta.url);

export const getData = () => JSON.parse(readFileSync(dbPath));
export const setData = (data) => writeFileSync(dbPath, JSON.stringify(data));

export const api = "https://pokeapi.co/api/v2/pokemon?offset=20&limit=50";
export const apiDetail = "https://pokeapi.co/api/v2/pokemon";
export const dataPokemon = axios.get(api);

export const generateId = () => {
  const randomString = Math.random().toString(16).slice(2);
  return randomString;
};

// for prime number
export const numberPrime = (number) => {
  if (number <= 1) {
    return false;
  }
  if (number <= 3) {
    return true;
  }
  if (number % 2 === 0 || number % 3 === 0) {
    return false;
  }
  for (let i = 5; i * i <= number; i += 6) {
    if (number % i === 0 || number % (i + 2) === 0) {
      return false;
    }
  }
  return true;
};

export const fibonacci = (index) => {
  if (index === 0) return 0;
  if (index === 1) return 1;

  let prev = 0;
  let current = 1;

  for (let i = 2; i <= index; i++) {
    const next = prev + current;
    prev = current;
    current = next;
  }

  return current;
};

export const renamePokemon = (name, release) => {
  const splitName = name.split("-");
  const originalName = splitName[0];
  const fiboKey = fibonacci(release);
  return `${originalName}-${fiboKey}`;
};

export const fibonacciFunction = () => {};
