import axios from "axios";
import pokemonController from "../Controllers/PokemonController";
import * as helper from "../Helper/helper.js";
import { generateId, getData } from "../Helper/helper.js";

jest.mock("axios");

describe("pokemonController", () => {
  // ...
  const mockApi = {
    api: "https://pokeapi.co/api/v2/pokemon?offset=20&limit=50",
  };

  const mockRes = {
    status: jest.fn(),
    send: jest.fn(),
  };

  // Test getPokemonAll

  it("should get all Pokemon successfully", async () => {
    // Mock axios.get dengan respons palsu
    const mockedApiResponse = {
      status: 200,
      data: "mocked-response-data",
    };
    axios.get.mockResolvedValue(mockedApiResponse);

    // Mock request dan response
    const mockReq = {};
    const mockRes = {
      status: jest.fn().mockReturnThis(),
      send: "Pokemon not found",
    };

    // Panggil fungsi getPokemonAll
    await pokemonController.getPokemonAll(mockReq, mockRes);

    // Ekspetasi bahwa axios.get telah dipanggil dengan URL yang benar
    expect(axios.get).toHaveBeenCalledWith.mockApi;

    // Ekspetasi bahwa respon telah dikirim dengan status 200 dan data yang tepat
    expect(mockRes.status).toHaveBeenCalledWith(404);
    expect(mockRes.send).toHaveBeenCalledWith("mocked-response-data");
  });

  it("should handle Pokemon not found", async () => {
    // Mock axios.get dengan respons yang menandakan Pokemon tidak ditemukan
    const mockedApiResponse = {
      status: 404,
    };
    axios.get.mockResolvedValue(mockedApiResponse);

    // Mock request dan response
    const mockReq = {};
    const mockRes = {
      status: jest.fn().mockReturnThis(),
      send: jest.fn(),
    };

    // Panggil fungsi getPokemonAll
    await pokemonController.getPokemonAll(mockReq, mockRes);

    // Ekspetasi bahwa axios.get telah dipanggil dengan URL yang benar
    expect(axios.get).toHaveBeenCalledWith.mockApi;

    // Ekspetasi bahwa respon telah dikirim dengan status 404 dan pesan yang tepat
    expect(mockRes.status).toHaveBeenCalledWith(404);
    expect(mockRes.send).toHaveBeenCalledWith("Pokemon not found");
  });

  it("should handle internal server error", async () => {
    // Mock axios.get dengan respons yang menandakan error
    const mockedApiResponse = {
      status: 404,
      data: {
        message: "Internal server error",
      },
    };
    axios.get.mockRejectedValue(mockedApiResponse);

    // Mock request dan response
    const mockReq = {};
    const mockRes = {
      status: jest.fn().mockReturnThis(),
      send: jest.fn(),
    };

    // Panggil fungsi getPokemonAll
    await pokemonController.getPokemonAll(mockReq, mockRes);

    // Ekspetasi bahwa axios.get telah dipanggil dengan URL yang benar
    expect(axios.get).toHaveBeenCalledWith.mockApi;

    expect(mockRes.status).toHaveBeenCalledWith(404);
    expect(mockRes.send).toHaveBeenCalledWith("Pokemon not found");
  });

  // Test getPokemonDetail
  it("should get Pokemon detail", async () => {
    const mockReq = {
      params: { pokemon: "charizard" },
    };
    const mockData = { name: "Charizard", types: ["Fire", "Flying"] };
    axios.get.mockResolvedValue({ data: mockData });

    await pokemonController.getPokemonDetail(mockReq, mockRes);

    expect(mockRes.status).toHaveBeenCalledWith(200);
    expect(mockRes.send).toHaveBeenCalledWith(mockData);
  });

  // Test getPokemonDetail when axios.get fails
  it("should handle error when getting Pokemon detail", async () => {
    const mockReq = {
      params: { pokemon: "charizard" },
    };
    const errorMessage = "Failed to fetch Pokemon detail";
    axios.get.mockRejectedValue(new Error(errorMessage));

    await pokemonController.getPokemonDetail(mockReq, mockRes);

    expect(mockRes.status).toHaveBeenCalledWith(500);
    expect(mockRes.send).toHaveBeenCalledWith(errorMessage);
  });

  it("should successfully catch a Pokemon", async () => {
    // Mock request dan response
    const mockReq = {
      body: { name: "Pikachu" },
    };
    const mockRes = {
      status: jest.fn().mockReturnThis(),
      send: jest.fn(),
    };

    // Mock generateId
    helper.generateId.mockReturnValue("mocked-id");

    // Mock getData
    const mockedData = {
      PokeDex: [],
    };
    helper.getData.mockReturnValue(mockedData);

    // Mock Math.random agar isSuccess menjadi true
    const originalMathRandom = Math.random;
    Math.random = jest.fn().mockReturnValue(0.4);

    // Panggil fungsi catchPokemon
    await pokemonController.catchPokemon(mockReq, mockRes);

    // Kembalikan Math.random ke keadaan semula
    Math.random = originalMathRandom;

    // Ekspetasi bahwa generateId telah dipanggil
    expect(helper.generateId).toHaveBeenCalled();

    // Ekspetasi bahwa getData telah dipanggil
    expect(helper.getData).toHaveBeenCalled();

    // Ekspetasi bahwa setData telah dipanggil dengan data yang tepat
    const expectedData = {
      ...mockedData,
      PokeDex: [
        ...mockedData.PokeDex,
        { id: mockedId, name: "Pikachu", release: 0 },
      ],
    };
    expect(require("../Helper/helper.js").setData).toHaveBeenCalledWith(
      expectedData
    );

    // Ekspetasi bahwa respon telah dikirim dengan status 200 dan pesan yang tepat
    expect(mockRes.status).toHaveBeenCalledWith(200);
    expect(mockRes.send).toHaveBeenCalledWith(
      "Gotcha ✨, You successfully caught Pikachu"
    );
  });

  it("should handle missing Pokemon name", async () => {
    // Mock request dan response
    const mockReq = {
      body: {},
    };
    const mockRes = {
      status: jest.fn().mockReturnThis(),
      send: jest.fn(),
    };

    // Panggil fungsi catchPokemon
    await pokemonController.catchPokemon(mockReq, mockRes);

    // Ekspetasi bahwa respon telah dikirim dengan status 400 dan pesan yang tepat
    expect(mockRes.status).toHaveBeenCalledWith(400);
    expect(mockRes.send).toHaveBeenCalledWith("Pokemon name is missing 🤔");
  });

  it("should handle failed Pokemon catch", async () => {
    // Mock request dan response
    const mockReq = {
      body: { name: "Charmander" },
    };
    const mockRes = {
      status: jest.fn().mockReturnThis(),
      send: jest.fn(),
    };

    // Mock generateId
    // require("../Helper/helper.js").generateId.mockReturnValue("mocked-id");

    // generateId.mockReturnValue("mocked-id");

    // Mock getData
    require("../Helper/helper.js").getData.mockReturnValue({
      PokeDex: [],
    });

    // Mock Math.random agar isSuccess menjadi false
    const originalMathRandom = Math.random;
    Math.random = jest.fn().mockReturnValue(0.8);

    // Panggil fungsi catchPokemon
    await pokemonController.catchPokemon(mockReq, mockRes);

    // Kembalikan Math.random ke keadaan semula
    Math.random = originalMathRandom;

    // Ekspetasi bahwa respon telah dikirim dengan status 400 dan pesan yang tepat
    expect(mockRes.status).toHaveBeenCalledWith(400);
    expect(mockRes.send).toHaveBeenCalledWith("Failed to catch Charmander");
  });

  it("should handle internal server error", async () => {
    // Mock request dan response
    const mockReq = {
      body: { name: "Bulbasaur" },
    };
    const mockRes = {
      status: jest.fn().mockReturnThis(),
      send: jest.fn(),
    };

    // Mock generateId
    require("../Helper/helper.js").generateId.mockReturnValue("mocked-id");

    // Mock getData yang akan melempar error
    require("../Helper/helper.js").getData.mockImplementation(() => {
      throw new Error("Mocked error");
    });

    // Panggil fungsi catchPokemon
    await pokemonController.catchPokemon(mockReq, mockRes);

    // Ekspetasi bahwa respon telah dikirim dengan status 500 dan pesan yang tepat
    expect(mockRes.status).toHaveBeenCalledWith(500);
    expect(mockRes.send).toHaveBeenCalledWith("Mocked error");
  });

  // Test getPokeDex
  it("should get PokeDex", () => {
    const mockPokeDex = [{ id: "123", name: "Bulbasaur" }];
    const mockGetData = jest.spyOn(helper, "getData");
    mockGetData.mockReturnValue({ PokeDex: mockPokeDex });

    const mockRes = {
      status: jest.fn(),
      send: jest.fn(),
    };
    const mockReq = {};

    pokemonController.getPokeDex(mockReq, mockRes);

    expect(mockRes.status).toHaveBeenCalledWith(200);
    expect(mockRes.send).toHaveBeenCalledWith(mockPokeDex);

    mockGetData.mockRestore();
  });

  // Test getPokemonRelease (prime number)
  it("should allow releasing Pokemon", () => {
    const mockReq = {};
    const mockRes = {
      status: jest.fn(),
      send: jest.fn(),
    };
    const mockNext = jest.fn();

    pokemonController.getPokemonRelease(mockReq, mockRes, mockNext);

    expect(mockNext).toHaveBeenCalled();
  });

  // Test getPokemonRelease (non-prime number)
  it("should prevent releasing Pokemon", () => {
    const mockReq = {};
    const mockRes = {
      status: jest.fn(),
      send: jest.fn(),
    };
    const mockRandom = jest.spyOn(Math, "random");
    mockRandom.mockReturnValue(0.3); // Non-prime number

    const mockNext = jest.fn();

    pokemonController.getPokemonRelease(mockReq, mockRes, mockNext);

    expect(mockRes.status).toHaveBeenCalledWith(200);
    expect(mockRes.send).toHaveBeenCalledWith("Pokemon cannot be released");

    mockRandom.mockRestore();
  });

  it("should call next() if the random number is prime", () => {
    // Mock request, response, dan next
    const mockReq = {};
    const mockRes = {
      status: jest.fn(),
      send: jest.fn(),
    };
    const mockNext = jest.fn();

    // Mock numberPrime agar mengembalikan true
    // require("../Helper/helper.js").numberPrime.mockReturnValue(true);

    // Panggil fungsi getPokemonRelease
    pokemonController.getPokemonRelease(mockReq, mockRes, mockNext);

    // Ekspetasi bahwa numberPrime telah dipanggil
    expect(helper.numberPrime).toHaveBeenCalled();

    // Ekspetasi bahwa next() telah dipanggil
    expect(mockNext).toHaveBeenCalled();

    // Ekspetasi bahwa res.status() dan res.send() tidak dipanggil
    expect(mockRes.status).not.toHaveBeenCalled();
    expect(mockRes.send).not.toHaveBeenCalled();
  });

  it("should return status 400 when the selected Pokemon is not found", () => {
    // Mock dataPokedex
    const mockDataPokedex = {
      PokeDex: [{ id: "12423", name: "Bulbasaur-1", release: 2 }],
    };
    require("../Helper/helper.js").getData.mockReturnValue(mockDataPokedex);

    // Mock req dan res
    const mockReq = { params: { idPokemon: "non-existent-id" } };
    const mockRes = {
      status: jest.fn().mockReturnThis(),
      send: jest.fn(),
    };

    // Panggil fungsi getEvolution
    pokemonController.getEvolution(mockReq, mockRes);

    // Ekspektasi respon
    expect(mockRes.status).toHaveBeenCalledWith(400);
    expect(mockRes.send).toHaveBeenCalledWith(
      "Pokemon tidak dapat ditemukan, 🤔"
    );
  });

  // Test getEvolution (success)
  it("should evolve Pokemon", () => {
    const mockReq = {
      params: { idPokemon: "123" },
    };

    const mockRes = {
      status: jest.fn(),
      send: jest.fn(),
    };

    const mockGetData = jest.spyOn(helper, "getData");
    const mockPokeDex = [
      { id: "123", name: "Bulbasaur", release: 0 },
      { id: "456", name: "Ivysaur", release: 1 },
    ];
    mockGetData.mockReturnValue({ PokeDex: mockPokeDex });

    pokemonController.getEvolution(mockReq, mockRes);

    expect(mockRes.send).toHaveBeenCalledWith(200);

    const expectedEvolvedPokemon = {
      id: "123",
      name: "Bulbasaur-1",
      release: 1,
    };
    expect(mockRes.send).toHaveBeenCalledWith(expectedEvolvedPokemon);

    mockGetData.mockRestore();
  });
});
