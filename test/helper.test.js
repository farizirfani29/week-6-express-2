import {
  generateId,
  numberPrime,
  fibonacci,
  renamePokemon,
} from "../Helper/helper.js";

describe("Helper Functions", () => {
  it("should calculate Fibonacci numbers correctly", () => {
    expect(fibonacci(0)).toBe(0);
    expect(fibonacci(1)).toBe(1);
    expect(fibonacci(2)).toBe(1);
    expect(fibonacci(3)).toBe(2);
    expect(fibonacci(4)).toBe(3);
  });
});

describe("generateId", () => {
  it("should generate a random ID", () => {
    const id = generateId();
    expect(typeof id).toBe("string");
    expect(id).toHaveLength(13);
  });
});

describe("numberPrime", () => {
  it("should return false for numbers less than or equal to 1", () => {
    expect(numberPrime(0)).toBe(false);
    expect(numberPrime(1)).toBe(false);
  });

  it("should return true for prime numbers less than or equal to 3", () => {
    expect(numberPrime(2)).toBe(true);
    expect(numberPrime(3)).toBe(true);
  });

  it("should correctly identify prime numbers", () => {
    expect(numberPrime(5)).toBe(true);
    expect(numberPrime(7)).toBe(true);
    expect(numberPrime(11)).toBe(true);
    expect(numberPrime(13)).toBe(true);
    expect(numberPrime(17)).toBe(true);
  });

  it("should correctly identify non-prime numbers", () => {
    expect(numberPrime(4)).toBe(false);
    expect(numberPrime(6)).toBe(false);
    expect(numberPrime(9)).toBe(false);
    expect(numberPrime(15)).toBe(false);
    expect(numberPrime(25)).toBe(false);
  });
});

describe("generateId", () => {
  it("should generate a random ID", () => {
    const id = generateId();
    expect(typeof id).toBe("string");
    expect(id).toHaveLength(13);
  });
});

describe("renamePokemon", () => {
  it("should rename Pokemon correctly", () => {
    // Mock fibonacci function
    const mockFibonacci = jest.fn((release) => release * 2);

    // Panggil fungsi renamePokemon dengan input yang tepat
    const originalName = "charizard";
    const release = 0;
    const newName = renamePokemon(originalName, release);

    // Ekspetasi hasil yang sesuai
    // expect(mockFibonacci).toHaveBeenCalledWith(release);
    expect(newName).toBe(`${originalName}-${release}`);
  });
});
