import express from "express";
import pokemonRouter from "./routes/pokemonRouter.js";

const port = 5000;
const app = express();

//
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/pokemon", pokemonRouter);

app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});
