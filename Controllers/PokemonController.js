import axios from "axios";
import {
  getData,
  setData,
  generateId,
  apiDetail,
  dataPokemon,
  numberPrime,
  renamePokemon,
} from "../Helper/helper.js";

const pokemonController = {
  getPokemonAll: async (req, res) => {
    try {
      const response = await dataPokemon;
      if (response) {
        res.status(200).send(response.data);
      } else {
        res.status(404).send("Pokemon not found");
      }
    } catch (error) {
      res.status(500).send(error.message);
    }
  },

  getPokemonDetail: async (req, res) => {
    try {
      const { pokemon } = req.params;
      const response = await axios.get(`${apiDetail}/${pokemon}`);
      const data = response.data;
      res.status(200).send(data);
    } catch (error) {
      res.status(500).send(error.message);
    }
  },

  catchPokemon: (req, res) => {
    try {
      const newPokemon = {
        id: generateId(),
        ...req.body,
        release: 0,
      };
      if (!newPokemon.name) {
        res.status(400).send("Pokemon name is missing 🤔");
        return;
      }
      const data = getData();
      const isSuccess = Math.random() < 0.5;
      if (isSuccess) {
        setData({ ...data, PokeDex: [...data.PokeDex, newPokemon] });
        res
          .status(200)
          .send(`Gotcha ✨, You successfully caught ${newPokemon.name}`);
      } else {
        res.status(400).send(`Failed to catch ${newPokemon.name}`);
      }
    } catch (error) {
      res.status(500).send(error.message);
    }
  },

  getPokeDex: (req, res) => {
    try {
      const pokeDexData = getData();
      const pokeDex = pokeDexData.PokeDex;
      res.status(200).send(pokeDex);
    } catch (error) {
      res.status(500).send(error.message);
    }
  },

  getPokemonRelease: (req, res, next) => {
    try {
      const randomNumber = Math.floor(Math.random() * 100);
      const isPrime = numberPrime(randomNumber);
      if (isPrime) {
        next();
      } else {
        res.status(200).send("Pokemon cannot be released");
      }
    } catch (error) {
      res.status(500).send(error.message);
    }
  },

  getEvolution: (req, res) => {
    try {
      const { idPokemon } = req.params;
      const dataPokedex = getData();
      const selectedPokemon = dataPokedex.PokeDex.find(
        (pokemon) => pokemon.id === idPokemon
      );
      if (selectedPokemon) {
        const updatedPokedex = dataPokedex.PokeDex.map((pokemon) => {
          if (pokemon.id === idPokemon) {
            pokemon.name = renamePokemon(pokemon.name, pokemon.release);
            pokemon.release += 1;
          }
          return pokemon;
        });
        setData({ ...dataPokedex, PokeDex: updatedPokedex });
        res.status(200).send(selectedPokemon);
      } else {
        res.status(400).send("Pokemon tidak dapat ditemukan, 🤔");
      }
    } catch (error) {
      res.status(500).send(error.message);
    }
  },
};

export default pokemonController;
