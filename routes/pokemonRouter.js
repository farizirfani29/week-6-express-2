import express from "express";
import pokemonController from "../Controllers/PokemonController.js";

const pokemonRouter = express.Router();

pokemonRouter.get("/", pokemonController.getPokemonAll);
pokemonRouter.get("/detail/:pokemon", pokemonController.getPokemonDetail);
pokemonRouter.post("/", pokemonController.catchPokemon);
pokemonRouter.get("/pokedex", pokemonController.getPokeDex);
pokemonRouter.put(
  "/evolution/:idPokemon",
  pokemonController.getPokemonRelease,
  pokemonController.getEvolution
);

export default pokemonRouter;
